# [Section] Input
# input() is similar to prompt() in JS that seeks to gather data from user input. returns string data type
# "\n" stands for line break
username = input("Please enter your name:\n")
print(f"Hi {username}! Welcome to Python Short Course!")

num1 = int(input("Please enter the first number\n"))
num2 = int(input("Please enter the second number\n"))
print(f"The sum of num1 and num2 is {num1 + num2}")

# [Section] If-Else Statements
test_num = 59
# in Python, the condition that the if statement has to assess is not enclosed in parenthesis (although it's not a requirement, it can still be used). at the end of the statement, there is a colon, denoting that indented statements below will be executed

# the rule for indention is also applicable for other code syntax such as functions (space can be used but it is better to use tab to see the indent better)

# if test_num >= 60 and test_num == 0 : - if we need to assess multiple conditions, use logical operators
if test_num >= 60:
    print("Test Passed")
else:
    print("Test Failed")

# if-else chains
test_num2 = int(input("Please enter a number for testing:\n"))
if test_num2 > 0:
    print("The number is positive")
# elif is the equal of "else if" keyword in JS
elif test_num2 == 0:
    print("The number is zero")
else:
    print("The number is negative")
"""
Miniactivity
- create an if-else statement that determines if a number is divisible by 3, 5, or both
- if it is divisible by 3, print "The number is divisible by 3"
- if it is divisible by 5, print "The number is divisible by 5"
- if it is divisible by 3 and 5, print "The number is divisible by 3 and 5"
- if it is not divisible by either 3 or 5, print "The number is not divisible by 3 nor 5"

6:32 pm, kindly send the output in our batch google chat
"""
test_div_num = int(input("Please enter a number:\n"))
if test_div_num % 3 == 0 and test_div_num % 5 == 0:
    print(f"{test_div_num} is divisible by 3 and 5")
elif test_div_num % 3 == 0:
    print(f"{test_div_num} is divisible by 3")
elif test_div_num % 5 == 0:
    print(f"{test_div_num} is divisible by 5")
else:
    print(f"{test_div_num} is not divisible by 3 nor 5")

# [Section] Loops

# [Section] While Loop
# performs a code block as long as the condition is true
i = 1
while i <= 5:
    print(f"Current Value: {i}")
    i += 1

# [Section] For Loop
# used to iterate through values or sequence
fruits = ["apple", "orange", "banana"]
"""
  [Section] For Loop + conditional statement
for indiv_fruit in fruits:
	if indiv_fruit == "apple":
		print(indiv_fruit)
"""
for indiv_fruit in fruits:
    print(indiv_fruit)

# range() method returns the sequence of the given number
# together with for loop, it will allow us to iterate through the range of values.
"""
SYNTAX:
	range(stop)
	range(start, stop)
	range(start, stop, step)
"""
# range_value = range(6)
# print(range_value)

for x in range(6):
    print(f"Current Value: {x}")

for x in range(1, 10):
    print(f"Current Value: {x}")

for x in range(1, 10, 2):
    print(f"Current Value: {x}")

# [Section] Break and Continue Statements
# Break statement
# break is used to stop the loop
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1

# Continue Statement
# continue statement is used to stop the current iteration and continue to the next iteration
# takes the control back to the top of the iteration
k = -1
while k < 6:
    k += 1  # signifies the top of the loop should we use continue statement
    if k == 3:
        continue
    print(k)

"""
the code below is not an infinite loop, but rather the continue statement is trying to find the "top" of the loop which is the "k += 1" that is skipped after the if statement

k = -1
while k < 6:
	if k == 3:
		continue
	print(k)
	k += 1
"""
