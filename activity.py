input_year = input("Please enter a year:\n")

if input_year.isnumeric():
    input_year = int(input_year)
    if input_year <= 0:
        print("Error Invalid Year.")
    elif input_year % 4 == 0:
        print(f"The year {input_year}, is a leap year.")

    elif input_year % 400 == 0:
        print(f"The year {input_year}, is a leap year.")

    else:
        print(f"The year {input_year}, is not a leap year.")
else:
    print("Error! Entered year is not an integer. Please try again.")


row = int(input("Enter first number:"))
col = int(input("Enter second number:"))
Symbol = input("Enter the chosen symbol")
row_counter = 1
column_counter = 1

if row <= 0 and col <= 0:
    print("Error! Please enter a number starting from 1.")
else:
    for row_counter in range(row):
        for column_counter in range(col):
            print(Symbol, end="")
        print()
